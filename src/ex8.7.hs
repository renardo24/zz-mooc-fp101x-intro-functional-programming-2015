-- run as mapM1 putChar ['a', 'b', 'c']

sequence' [] = return []
sequence' (m: ms)
  = do a <- m
       as <- sequence' ms
       return (a : as)

sequence_' ms = foldr (>>) (return ()) ms

mapM1 f as = sequence' (map f as)

mapM2 f [] = return []
mapM2 f (a: as)
  = f a >>= \ b -> mapM2 f as >>= \ bs -> return (b :bs)

mapM3 f as = sequence_' (map f as)

--mapM4 f [] = return []
--mapM4 f (a : as)
--  = f a >> \ b -> mapM4 f as >> \ bs -> return (b : bs)

--mapM5 f = return []
--mapM5 f (a : as) =
--  do
--       f a -> b
--       mapM5 f as -> bs
--       return (b : bs)

--mapM6 f = return []
--mapM6 f (a : as)
--  = do b <- f a
--       bs <- mapM6 f as
--       return (b : bs)

mapM7 f [] = return []
mapM7 f (a : as)
  = f a >>=
      \ b ->
        do bs <- mapM7 f as
           return (b : bs)

mapM8 f [] = return []
mapM8 f (a : as)
  = f a >>=
      \ b ->
        do bs <- mapM8 f as
           return (bs ++ [b])


