-- Choose the correct definition for the function merge :: Ord a => [a] -> [a] -> [a] that merges two sorted lists in ascending order to give a single sorted list in ascending order. For example:
-- > merge [2, 5, 6] [1, 3, 4]
-- [1, 2, 3, 4, 5, 6]

merge1 [] ys = ys
merge1 xs [] = xs
merge1  (x:xs) (y:ys)
  = if x <= y then x : merge1 xs ys else y : merge1 xs ys

merge2 [] ys = ys
merge2 xs [] = xs
merge2 (x:xs) (y:ys)
  = if x <= y then y : merge2 xs (y:ys) else x : merge2 (x:xs) ys

merge3 [] ys = ys
merge3 xs [] = xs
merge3 (x:xs) (y:ys)
  = if x <= y then y : merge3 (x:xs) ys else x : merge3 xs (y:ys)

merge4 [] ys = ys
merge4 xs [] = xs
merge4 (x:xs) (y:ys)
  = if x <= y then x : merge4 xs (y:ys) else y : merge4 (x:xs) ys


