import Prelude hiding ((!!))

-- example
-- [1,2,3] !! 2
-- 3

-- (x : _) !! 1 = x
-- (_ : xs) !! n = xs !! (n - 1)

--(x : _) !! 0 = x
--(_ : xs) !! n = xs !! (n - 1)
--[] !! n = 0

--(x : _) !! 0 = x
--(_ : xs) !! n = xs !! (n - 1)

(x : _) !! 0 = [x]
(_ : xs) !! n = xs !! (n - 1)

