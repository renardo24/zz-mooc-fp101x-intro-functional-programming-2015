-- Choose a definition for the function dec2int :: [Integer] -> Integer that converts a finite, non-partial list of non-bottom Integer digits, that represents a decimal number, into the non-bottom Integer this list represents. For example:
-- > dec2int [2, 3, 4, 5]
-- 2345
-- > dec2int []
-- 0
-- > dec2int [0, 0, 0, 0]
-- 0

dec2int1 = foldr (\ x y -> 10 * x + y) 0

dec2int2 = foldl (\ x y -> x + 10 * y) 0

dec2int3 = foldl (\ x y -> 10 * x + y) 0

dec2int4 = foldr (\ x y -> x + 10 * y) 0

