-- Which of the following implementation defines a function getLine' :: IO String that reads a line, up to the first \n character, from the standard input?
-- Note: The helper function getChar :: IO Char reads a single character from the standard input.
-- To echo the output to stdout of what getLineX does with the input back, use this:
-- getLine1 >>= putStrLn

getLine1 = get1 ""
get1 :: String -> IO String
get1 xs
  = do x <- getChar
       case x of
           ' ' -> return xs
           '\n' -> return xs
           _ -> get1 (xs ++ [x])

getLine2 = get2 ""
get2 :: String -> IO String
get2 xs
  = do x <- getChar
       case x of
           '\n' -> return xs
           _ -> get2 (x:xs)

getLine3 = get3 []
get3 :: String -> IO String
get3 xs
  = do x <- getChar
       case x of
           '\n' -> return xs
           _ -> get3 (xs ++ [x])

getLine4 = get4 []
get4 :: String -> IO String
get4 xs
   = do x <- getChar
        case x of
            '\n' -> return (x : xs)
            _ -> get4 (xs ++ [x])
