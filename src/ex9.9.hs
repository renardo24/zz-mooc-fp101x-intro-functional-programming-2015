
-- original statement
data Maybe1 a = Nothing1 | Just1 a


{-
-- answer a
instance Monad Maybe1 where
    return x = Just1 x
    Nothing1 >>= f = Just1 (f (Nothing1))
    (Just1 x) >>= f = f x
-}

{-
-- answer b
instance Monad Maybe1 where
    return x = Just1 x
    Nothing1 >>= _ = Nothing1
    (Just1 x) >>= f = f x
-}

{-
-- answer c
instance Monad Maybe1 where
    return x = Just1 x
    Nothing1 >>= _ = Nothing1
    (Just1 x) >>= f = Just1 (f x)
-}

{-
-- answer d
instance Monad Maybe1 where
    return x = Just1 x
    Nothing1 >>= f = f Nothing1
    (Just1 x) >>= f = f x
-}
