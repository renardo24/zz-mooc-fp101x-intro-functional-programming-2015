-- The following list comprehension:
-- [(x, y) | x <- [1,2,3], y <- [4,5,6]]
--can be re-expressed using two or more comprehensions with single generators. Choose the implementation that is equivalent to the one above.

[z | z <- [[(x, y) | y <- [4,5,6]] | x <- [1,2,3]]]

concat [[[(x, y)] | x <- [1,2,3]] | y <- [4,5,6]]

-- concat [(x, y) | y <- [4,5,6]] | x <- [1,2,3]

concat [[(x,y) | y <- [4,5,6]] | x <- [1,2,3]]

