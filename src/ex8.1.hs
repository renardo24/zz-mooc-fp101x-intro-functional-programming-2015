--Which of the following implementations defines a function putStr' :: String -> IO () that takes a String as its parameter and writes it to the standard output?
--Note: The helper function putChar :: Char -> IO () takes a character as its parameter and writes it to the standard output.
putStr1 [] = return ""
putstr1 (x:xs) = putChar x >> putStr1 xs

putStr2 [] = return ()
putStr2 (x:xs) = putChar x >> putStr2 xs

--putStr3 [] = return ()
--putStr3 (x:xs) = putChar x >>= putStr3 xs

--putStr4 [] = return ()
--putStr4 (x:xs) = putStr xs >>= putChar x

