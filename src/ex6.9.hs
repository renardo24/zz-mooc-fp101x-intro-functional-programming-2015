-- Choose the correct definition for the Prelude function curry :: ((a, b) -> c) -> a -> b -> c , that converts a function that takes its arguments as a pair into a function that takes its arguments one at a time. For this exercise assume that bottom does not exist. 

curry1 f = \ x y -> f x y

curry2 f = \ x y -> f

curry3 f = \ x y -> f (x, y)

curry4 f = \ (x, y) -> f x y

-- call with aaa 2 3
-- result = 5
aaa = curry (\ (x, y) -> sum [x,y])

