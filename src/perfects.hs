-- A positive integer is perfect if it equals the sum of its factors,
-- excluding the number itself. Choose the correct definition of the function 
-- perfects :: Int -> [Int] that returns the list of all perfect numbers up to a given limit.

-- Note: factors is not a library function but is defined in the lecture.

factors :: Int -> [Int]
factors n = [x | x <- [1..n], n `mod` x == 0]

perfects1 n = [x | x <- [1..n], isPerfect x]
  where isPerfect num = sum (factors num) == num

perfects2 n = [x | x <- [1..n], isPerfect x]
  where isPerfect num = sum (init (factors num)) == num

perfects3 n = [isPerfect x | x <- [1..n]]
  where isPerfect num = sum (init (factors num)) == num

--perfects4 n = [x | x <- [1..n], isPerfect x]
--  where isPerfect num = init (factors num) == num

