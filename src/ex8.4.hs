putStr' [] = return ()
putStr' (x : xs) = putChar x >> putStr' xs

putStrLn' [] = putChar '\n'
putStrLn' xs = putStr' xs >> putStr' "\n"

getLine' = []
get :: String -> IO String
get xs
 = do x <- getChar
      case x of
          '\n' -> return xs
          _ -> get (xs ++ [x])

--interact1 f
--  = do input <- getLine'
--       putStrLn' (f input)

--interact2 f
--  = do input <- getLine'
--       putStrLn' input

interact3 f
  = do input <- getChar
       putStrLn' (f input)

--interact4 f
--  = do input <- getLine'
--       putStr' (f input)
