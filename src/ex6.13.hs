-- Consider the following higher-order function that encapsulates a simple pattern of recursion for producing a list.
-- The function unfold p h t x produces the empty list if the predicate p x is True. Otherwise it produces a non-empty list by applying the function h x to give the head of the generated list, and the function t x to generate another seed that is recursively processed by unfold to produce the tail of the generated list.
unfold :: (b -> Bool) -> (b -> a) -> (b -> b) -> b -> [a]
unfold p h t x
  | p x = []
  | otherwise = h x : unfold p h t (t x)


-- Choose an implementation of the Prelude function iterate :: (a -> a) -> a -> [a] using unfold.

iterate1 f = unfold (const False) id f
iterate2 f = unfold (const False) f f
iterate3 f = unfold (const True) id f
iterate4 f = unfold (const True) f f

