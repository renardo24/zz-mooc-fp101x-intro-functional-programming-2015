-- which one behaves like the library function last?
last1 xs = drop (length xs - 1 ) xs -- returns 1-element array
last2 xs = head (drop (length xs - 1) xs) -- OK
last3 xs = tail (reverse xs) -- returns all but the first element
last4 xs = reverse (head xs) -- error
last5 xs = xs !! (length xs - 1) -- OK
last6 xs = head (drop (length xs) xs) -- error, calling head on empty list
last7 xs = head (reverse xs) -- OK
last8 xs = reverse xs !! (length xs - 1) -- nope
last9 xs = reverse xs !! 0 -- OK
