-- called with: sequence5_' [putChar 'a', putChar 'b']
sequence1_' [] = return ()
sequence1_' (m : ms) = (foldl (>>) m ms) >> return ()

sequence2_' [] = return ()
sequence2_' (m : ms) = (foldl (>>) m ms) >> return ()

sequence3_' ms = foldl (>>) (return ()) ms

sequence4_' [] = return ()
sequence4_' (m : ms) = m >> sequence4_' ms

sequence5_' [] = return ()
sequence5_' (m : ms) = m >>= \ _ -> sequence5_' ms

--sequence6_' ms = foldr (>>=) (return ()) ms

sequence7_' ms = foldr (>>) (return ()) ms

sequence8_' ms = foldr (>>) (return []) ms


