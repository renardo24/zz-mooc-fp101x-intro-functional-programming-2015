-- replicate :: Int -> a -> [a]

replicateA n a = [True | _ <- [1..n]]

replicateB n a = [n | _ <- [1..n]]

replicateC n a = [a | _ <- [1..a]]

replicateD n a = [a | _ <- [1..n]]
