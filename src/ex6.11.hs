-- Consider the following higher-order function that encapsulates a simple pattern of recursion for producing a list.
-- The function unfold p h t x produces the empty list if the predicate p x is True. Otherwise it produces a non-empty list by applying the function h x to give the head of the generated list, and the function t x to generate another seed that is recursively processed by unfold to produce the tail of the generated list.
unfold :: (b -> Bool) -> (b -> a) -> (b -> b) -> b -> [a]
unfold p h t x
  | p x = []
  | otherwise = h x : unfold p h t (t x)

type Bit = Int

int2bin :: Int -> [Bit]
int2bin 0 = []
int2bin n = n `mod` 2 : int2bin (n `div` 2)

int2bin2 = unfold (==0) (`mod` 2) (`div` 2)

chop8 :: [Bit] -> [[Bit]]
chop8 [] = []
chop8 bits = take 8 bits : chop8 (drop 8 bits)

--chop8b = unfold [] (drop 8) (take 8)
chop8c = unfold null (take 8) (drop 8)
chop8d = unfold null (drop 8) (take 8)
chop8e = unfold (const False) (take 8) (drop 8)

