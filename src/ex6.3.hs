-- Choose the option that implements the Prelude function
-- takeWhile :: (a -> Bool) -> [a] -> [a]
-- taking into account only finite, non-partial input lists with non-bottom values and where the predicate p always returns either True, or False, but not bottom.

takeWhile1 _ [] = []
takeWhile1 p (x : xs)
  | p x = x : takeWhile1 p xs
  | otherwise = takeWhile1 p xs

takeWhile2 _ [] = []
takeWhile2 p (x : xs)
  | p x = x : takeWhile2 p xs
  | otherwise = []

takeWhile3 _ [] = []
takeWhile3 p (x : xs)
  | p x = takeWhile3 p xs
  | otherwise = []

takeWhile4 p = foldl (\ acc x -> if p x then x : acc else acc) []




