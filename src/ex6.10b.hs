-- Choose the definition for the Prelude function uncurry :: (a -> b -> c) -> (a, b) -> c, that converts a function that takes its arguments one at a time into a function that takes its arguments as a pair. For this exercise assume that bottom does not exist. 

uncurry1 f = \ (x, y) -> f x y

uncurry2 f = \ x y -> f (x, y)

uncurry3 f = \ (x, y) -> f

uncurry4 f = \ x y -> f

