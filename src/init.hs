-- which does the same thing as the library function init,
-- which removes the last element of a non-empty list
init1 xs = tail (reverse xs) -- nope
init2 xs = reverse (head (reverse xs)) -- error
init3 xs = reverse (tail xs) -- nope
init4 xs = take (length xs) xs -- nope
init5 xs = reverse (tail (reverse xs))
init6 xs = take (length xs - 1) (tail xs) -- nope
init7 xs = drop (length xs - 1) xs -- nope
