func xs = [id x | x <- xs, even x]

func2 xs = map even (map id xs)
func3 xs = filter even (map id xs)
func4 xs = map id (filter even xs)
func5 xs = map id (takeWhile even xs)
