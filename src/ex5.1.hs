-- Choose all correct definitions of the exponentiation operator ^ for non-negative integers (including 0).

-- does not work -> Syntax error in import declaration (unexpected symbol "^")
--import Prelude hiding (^)
-- this does, though
import Prelude hiding ((^))

--m ^ 0 = 0
--m ^ n = m * m ^ (n - 1)

--m ^ 0 = 1
--m ^ n = m * m ^ (n - 1) 

--m ^ 0 = 1
--m ^ n = m * m ^ n - 1

--m ^ 0 = 1
--m ^ n = n * n ^ (m - 1)

--m ^ 0 = 1
--m ^ n = m * (^) m (n - 1)

--m ^ 0 = 1
--m ^ n = m * m ^ (n - 2)

--m ^ 0 = 1
--m ^ n = (m * m) ^ (n - 1)

m ^ 1 = m
m ^ n = m * m ^ (n - 1)
