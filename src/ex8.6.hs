-- Called with sequence1' [putChar 'a', putChar 'b']
sequence1 [] = return []
sequence1 (m : ms)
  = m >>=
      \ a ->
        do as <- sequence1 ms
           return (a : as)

--sequence2 ms = foldr func (return ()) ms
--  where
--         func :: (Monad m) => m a -> m [a] -> m [a]
--         func m acc
--           = do x <- m
--                xs <- acc
--                return (x : xs)

--sequence3 ms = foldr func (return []) ms
--  where
--         func :: (Monad m) => m a -> m [a] -> m [a]
--         func m acc = m : acc

--sequence4 [] = return []
--sequence4 (m : ms) = return (a : as)
--    where
--        a <- m
--        as <- sequence4 ms

sequence5 ms = foldr func (return []) ms
  where
        func :: (Monad m) => m a -> m [a] -> m [a]
        func m acc
          = do x <- m
               xs <- acc
               return (x : xs)

--sequence6 [] = return []
--sequence6 (m : ms)
--  = m >>
--      \ a ->
--        do as <- sequence6 ms
--           return (a : as)

--sequence7 [] = return []
--sequence7 (m : ms) = m >>= \a ->
--    as <- sequence7 ms
--    return (a : as)

sequence8 [] = return []
sequence8 (m : ms)
    = do a <- m
         as <- sequence8 ms
         return (a : as)

