-- Choose the option that implements the Prelude function
-- filter :: (a -> Bool)( -> [a] -> [a]
-- taking into account only finite, non-partial input lists with non-bottom values and where the predicate p always returns either True, or False, but not bottom.

filter1 p = foldl (\ xs x -> if p x then x : xs else xs) []

filter2 p = foldr (\ x xs -> if p x then x : xs else xs) []

filter3 p = foldr (\ x xs -> if p x then xs ++ [x] else xs) []

-- no valid syntax
--filter4 p = foldl (\ x xs -> if p x then xs ++ [x] else xs) []

