h g f = (f .g) $ f

-- what is the type of h
--((a -> b) -> a) -> ((a -> b) -> b)

fix = h fix
-- what is the type of fix
--(a -> a) -> a
