-- Consider the following higher-order function that encapsulates a simple pattern of recursion for producing a list.
-- The function unfold p h t x produces the empty list if the predicate p x is True. Otherwise it produces a non-empty list by applying the function h x to give the head of the generated list, and the function t x to generate another seed that is recursively processed by unfold to produce the tail of the generated list.
unfold :: (b -> Bool) -> (b -> a) -> (b -> b) -> b -> [a]
unfold p h t x
  | p x = []
  | otherwise = h x : unfold p h t (t x)


-- Following the previous question, choose an implementation of map :: (a -> b) -> [a] -> [b] using unfold, taking into account only finite, non-partial input lists with non-bottom values, and where the mapping function does not return bottom.

map1 f = unfold null (f) tail
map2 f = unfold null (f (head)) tail
map3 f = unfold null (f . head) tail
--map4 f = unfold empty (f . head) tail 
