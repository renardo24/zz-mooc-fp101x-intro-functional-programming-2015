-- Choose the option that implements the Prelude function
-- map :: (a -> b) -> [a] -> [b]
-- taking into account only finite, non-partial input lists with non-bottom values and where the mapping function does not return bottom.

map1 f = foldr (\ x xs -> xs ++ [f x]) []

map2 f = foldr (\ x xs -> f x ++ xs) []

map3 f = foldl (\ xs x -> f x : xs) []

map4 f = foldl (\ xs x -> xs ++ [f x]) []

