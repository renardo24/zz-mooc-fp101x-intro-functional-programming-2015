--sum100a = sum [[x * x] | x <- [1 .. 100]] -- does not compile
sum100b = sum [x ^ 2 | x <- [1 .. 100]] -- 338350
sum100c = sum [const 2 x | x <- [1 .. 100]] -- 200
sum100d = foldl (+) (1) [x ^ 2 | x <- [1 .. 100]] -- 338351

