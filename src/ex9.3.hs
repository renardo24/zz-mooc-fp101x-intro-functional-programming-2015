-- mult2 (Succ (Succ Zero)) (Succ (Succ Zero))
-- mult2 (Succ Zero) (Succ (Succ Zero))
-- mult2 (Succ Zero) (Succ Zero)
-- mult2 Zero Zero
-- mult2 Zero (Succ Zero)
-- Main> natToInteger (mult2 (Succ (Succ Zero)) (Succ (Succ Zero)))
--4
--Main> natToInteger (Succ (Succ Zero)) * natToInteger (Succ (Succ Zero))
--4

module Main where
import Data.List
import Data.Char
import Hugs.IOExts (unsafeCoerce)

data Nat = Zero
         | Succ Nat
         deriving Show

natToInteger (Succ n) = 1 + natToInteger n
natToInteger Zero = 0

add n (Succ m) = Succ (add m n)
add n Zero = n

mult1 Zero Zero = Zero
mult1 m (Succ n) = add m (mult1 m n)

mult2 m Zero = Zero
mult2 m (Succ n) = add m (mult2 m n)

mult3 m Zero = Zero
mult3 m (Succ n) = add n (mult3 m n)

mult4 m Zero = Zero
mult4 m n = add m (mult4 m (Succ n))

