--import Prelude hiding (and)

-- examples
--and6 [False, False, False]
--False
--
--and6 []
--True
--
--and6 [True, True, True]
--True
--
--and6 [False, True, True]
--False


and1 [] = True
and1 (b:bs) = b && and1 bs

and2 [] = True
and2 (b:bs)
  | b = and2 bs
  | otherwise = False

and3 [] = False
and3 (b:bs) = b && and3 bs

and4 [] = False
and4 (b:bs) = b || and4 bs

and5 [] = True
and5 (b:bs)
  | b == False = False
  | otherwise = and5 bs

and6 [] = True
and6 (b:bs) = and6 bs && b

and7 [] = True
and7 (b:bs) = and7 bs && b

and8 [] = True
and8 (b:bs)
  | b = b
  | otherwise = and8 bs

