-- Choose a suitable definition for the following type:
-- e15 :: [a] -> [b] -> (a, b)
e15a a b = (a, b)
e15b x y = (y, x)
e15c xs ys = (head xs, head ys)
e15d a b = [a, b]

