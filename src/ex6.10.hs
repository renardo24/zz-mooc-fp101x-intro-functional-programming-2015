add :: Integer -> Integer -> Integer -> Integer -> Integer 
add m n o p = m + n + o + p

add' :: Integer -> Integer -> Integer
add' m n = m + n

add'' :: Integer -> (Integer -> Integer)
add'' m n = m + n
