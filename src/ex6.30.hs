--filter p (map f xs)
filter even (map (*3) [1,2,3])

--map f (filter p xs)
map (*3) (filter even [1,2,3])

-- f [x|x <- xs, p x]
--(*3) [x | x <- [1,2,3], even x]

--[p (f x) | x <- xs]
[even ((*3)) x | x <- [1,2,3]]

--[f x | x <- xs, p (f x)]
[(*3) x | x <- [1,2,3], even ((*3) x)]

--[f x | x <- xs, p x]
[(*3) x | x <- [1,2,3], even x]

