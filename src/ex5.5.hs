-- example
-- concat [[1,2],[2,3],[3,4]]
-- [1,2,2,3,3,4]
concat1 [] = []
concat1 (xs : xss) = xs : concat1 xss

concat2 [] = []
concat2 (xs : xss) = xs ++ concat2 xss

concat3 [] = [[]]
concat3 (xs : xss) = xs ++ concat3 xss

concat4 [[]] = []
concat4 (xs : xss) = xs ++ concat4 xss

