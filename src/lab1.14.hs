-- Choose a suitable definition for the following type:
-- e14 :: ([Char], [Float])
e14a = [('C', 0.1)]
e14b = (['c', 'u', 'r', 'r', 'y'], 0.0)
e14c = (["Haskell"], [1.1, 2.2, 3.3])
e14d = ("Haskell", [3.1, 3.14, 3.141, 3.1415])

