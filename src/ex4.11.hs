xs where xs = 1 : [x + 1 | x <- xs]
let xs = 1 : [x + 1 | x <- xs] in xs
