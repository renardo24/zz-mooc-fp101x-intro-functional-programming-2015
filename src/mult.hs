mult x y z = x * y * z

mult1 x y z = \ x -> (\ y -> (\ z -> x * y * z))

-- mult2 = \ x -> (x * \ y -> (y * \ z -> z)) 

mult3 = \ x -> (\ y -> (\z -> x * y * z))

-- mult4 = ((((\x -> \y) -> \z) -> x * y) * z)

